package com.example.jaeyoungchu.mvppatternex.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jaeyoungchu.mvppatternex.R;
import com.example.jaeyoungchu.mvppatternex.controllers.MainPresenter;
import com.example.jaeyoungchu.mvppatternex.model.ITaskRepository;
import com.example.jaeyoungchu.mvppatternex.views.IMainView;

public class MainFragment extends Fragment implements IMainView {

    private MainPresenter mainPresenter;

    private TextView textView_title;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,container,false);
        textView_title = (TextView) rootView.findViewById(R.id.textView_title);

        mainPresenter = new MainPresenter();
        mainPresenter.attachView(this);
        mainPresenter.loadTask();
        return rootView;
    }

    @Override
    public void setTaskTitle(String text) {
        textView_title.setText(text);
    }
}
