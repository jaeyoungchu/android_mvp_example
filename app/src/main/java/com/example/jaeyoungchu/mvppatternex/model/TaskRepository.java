package com.example.jaeyoungchu.mvppatternex.model;

import java.util.HashMap;

public class TaskRepository implements ITaskRepository{
    private  static TaskRepository instance;
    private HashMap<Integer,Task> tasks =new HashMap<>();

    private TaskRepository(){

    }
    public static TaskRepository getInstance(){
        if (instance == null){
            instance = new TaskRepository();
        }
        return  instance;
    }

    public void insert(){

    }

    @Override
    public Task getTask(int id) {
        Task task = new Task();
        task.setTitle("title is : ");
        task.setContent("content");
        return task;
    }
}
