package com.example.jaeyoungchu.mvppatternex.model;

public interface ITaskRepository {
    Task getTask(int id);
}
