package com.example.jaeyoungchu.mvppatternex.controllers;

import com.example.jaeyoungchu.mvppatternex.model.ITaskRepository;
import com.example.jaeyoungchu.mvppatternex.model.Task;
import com.example.jaeyoungchu.mvppatternex.model.TaskRepository;
import com.example.jaeyoungchu.mvppatternex.views.IMainView;

public class MainPresenter {

    private IMainView view;
    private ITaskRepository taskRepository;

    public void attachView(IMainView view){
        this.view = view;
        taskRepository = TaskRepository.getInstance();
    }
    public void loadTask(){
        Task task = taskRepository.getTask(1);
        String titleToShow = task.getTitle() + "..";
        view.setTaskTitle(titleToShow);
    }
}
